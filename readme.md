# Projekt Goethe-Generator (Anfang 2014)

Im Rahmen einer Arbeit über die Standard Template Library programmierte ich den Goethegenerator. Dieser zeigt die Funktionsweise der STL und die Möglichkeiten, die dem Programmierer zur Verfügung stehen.
Der Goethe-Generator nimmt zwei Worte als Eingabe entgegen und generiert aus diesem Satzanfang einen Satz, der im Stil Goethes Literatur ähnelt, allerdings auf unterhaltsame Art semantisch keinen Sinn macht.

## Verwendete Technologien

* C++
* Standard Template Library

## Kurzbeschreibung

Der Goethe-Generator liest zu Beginn eine Textdatei mit einer Sammlung von Goethes Werken ein. Dabei erstellt er eine Struktur, in der immer zwei aufeinander folgende Wörter als Schlüssel, und das darauf folgende Wort als Wert gespeichert wird. Da in der Textdatei zwei aufeinanderfolgende Worte mehrmals vorkommen, ergeben sich auch mehrere Möglichkeiten, auf die der Satz fortgesetzt werden kann. Diese Möglichkeiten werden alle abgespeichert.
Beim Generieren startet der Algorithmus mit zwei zufälligen Wörtern oder mit einer Eingabe, such diese als Schlüssel und sucht aus den möglichen Wörtern eines aus. Dieses wird an das Ende des Satzes gehängt, der nun aus drei Wörtern besteht. Die letzten beiden Wörter werden nun wieder als Suchschlüssel verwendet, und das nächste Wort wird gesucht.

##Weiterführende Ressourcen

-