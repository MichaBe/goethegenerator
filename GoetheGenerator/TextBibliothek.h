#include <string>
#include <vector>
#include <fstream>
#include <algorithm>
#include <map>
#include <time.h>
using namespace std;

class TextBibliothek
{
public:
	TextBibliothek(void);
	~TextBibliothek(void);

	void LoadText(string path);
	void generateSecondStage();

	pair<string,string> getRandBegin();

	int possibilityCount(pair<string,string> proof);
	int possibilityCount(string first, string second);

	string getRand(pair<string,string> before);
	string getRand(string first, string second);

private:
	int state;

	static const int S_NULL = 0;
	static const int S_FIRST = 1;
	static const int S_SECOND = 2;
	
	vector<string> text_first_step;
	map<pair<string, string>, vector<string>> text_second_step;
};

