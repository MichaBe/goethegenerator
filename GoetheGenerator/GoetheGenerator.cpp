#include "stdafx.h"
#include <Windows.h>
#include "TextBibliothek.h"
#include <iostream>
using namespace std;


int main(int argc, char* argv[])
{
	TextBibliothek GoetheGenerator;
	int iTextAuswahl = 0;
	do {
		system("cls");
		cout << "Welche Textdatei m�chten Sie laden?\n";
		cout << "1 -> AllShakespeare.txt\n2 -> AllGoethe.txt\t";
		cin >> iTextAuswahl;
	} while (iTextAuswahl != 1 && iTextAuswahl != 2);
	string sPath = iTextAuswahl == 1 ? "AllShakespeare.txt" : "AllGoethe.txt";
	int tBefore = timeGetTime();
	GoetheGenerator.LoadText(sPath);

	int t1 = timeGetTime() - tBefore;
	GoetheGenerator.generateSecondStage();
	int t2 = timeGetTime() - t1 - tBefore;

	string ein;
	cout <<"Laden der Datei: "<<t1 <<"ms\nErstellen des Verzeichnisses: " <<t2 <<"ms"<<endl;
	
	do {
		pair<string,string> search;
		int auswahl = 0;

		cout <<"1 -> zwei Woerter selbst eingeben\n2 -> zwei zufaellige Woerter aussuchen:\t";
		cin >> auswahl;
		switch (auswahl) {
		case 1:
			cout <<"Bitte gib das erste Wort ein (nur Grossbuchstaben): ";
			cin >> search.first;
			cout <<"Bitte gib das zweite Wort ein (nur Grossbuchstaben): ";
			cin >> search.second;
			break;
		case 2:
			search = GoetheGenerator.getRandBegin();
			break;
		default:
			search = GoetheGenerator.getRandBegin();
			break;
		}

		string result = "";
		cout <<search.first <<" " <<search.second <<" ";
		do {
			do {
				if(GoetheGenerator.possibilityCount(search) > 0) {
					result = GoetheGenerator.getRand(search);
					cout <<result <<" ";
					search.first = search.second;
					search.second = result;
				}
				else
					break;
			}while(result.at(result.length()-1) != '!' 
				&& result.at(result.length()-1) != '?' 
				&& result.at(result.length()-1) != '.');
			cin >> ein;
		}while(ein != "E");
		cin >> ein;
		system("cls");
	}while(ein != "E");
	return 0;
}
