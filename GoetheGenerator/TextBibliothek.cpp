#include "TextBibliothek.h"


TextBibliothek::TextBibliothek(void) {
	srand(time(NULL));
	state = S_NULL;
}


TextBibliothek::~TextBibliothek(void) {
	text_first_step = vector<string>();
	text_second_step = map<pair<string,string>,vector<string>>();
}

void TextBibliothek::LoadText(string path) {
	ifstream textFile(path, ios::in);

	if(textFile) {
		string temp;
		text_first_step.reserve(820000);
		int i = 0;
		while(textFile >> temp) {
			transform(temp.begin(), temp.end(), temp.begin(), toupper);
			text_first_step.push_back(temp);
			i++;
		}
		textFile.close();
		state = S_FIRST;
	}
	else
		state = S_NULL;
}

void TextBibliothek::generateSecondStage() {
	if(state == S_FIRST) {
		string first, second, newEntry;
		for(int i = 0; i < text_first_step.size()-2; i++) {
			first = text_first_step.at(i);
			second = text_first_step.at(i+1);
			newEntry = text_first_step.at(i+2);
			text_second_step[pair<string,string>(first, second)].push_back(newEntry);
		}
		state = S_SECOND;
	}
}

int TextBibliothek::possibilityCount(pair<string,string> proof) {
	int returner = 0;
	if(state == S_SECOND) {
		if(text_second_step.count(proof) == 1)
			returner = text_second_step.at(proof).size();
	}
	return returner;
}

int TextBibliothek::possibilityCount(string first, string second) {
	return possibilityCount(pair<string,string>(first,second));
}

string TextBibliothek::getRand(pair<string,string> before) {
	string returner = "";
	int ct = possibilityCount(before);
	if(ct != 0) {
		returner = text_second_step.at(before).at(rand()%ct);
	}
	return returner;
}

string TextBibliothek::getRand(string first, string second) {
	return getRand(pair<string,string>(first,second));
}

pair<string,string> TextBibliothek::getRandBegin() {
	int compCount = text_second_step.size();
	int myRand = rand()%compCount;
	
	map<pair<string,string>,vector<string>>::iterator myIT = text_second_step.begin();
	
	for(int i = 0; i < myRand; i++)
		myIT++;

	return myIT->first;
}